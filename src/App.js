import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';

import { Button, Header, Spinner } from './components/common';
import LoginForm from './components/LoginForm';


class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDaun3HF9_mlbsfWCiUICtZ0FhKwE1fJqA',
      authDomain: 'auth-with-reactnative.firebaseapp.com',
      databaseURL: 'https://auth-with-reactnative.firebaseio.com',
      projectId: 'auth-with-reactnative',
      storageBucket: 'auth-with-reactnative.appspot.com',
      messagingSenderId: '707845448038'
    });

    firebase.auth().onAuthStateChanged((user) => {
      // If user exists.
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
      return (
          <Button onPress={() => firebase.auth().signOut()}>Log Out</Button>
        );
      case false:
        return <LoginForm />;
      default:
        return <Spinner size="large" />;
      }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
